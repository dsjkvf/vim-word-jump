" Helpers
" collect all the occurrences of a pattern in a line
function! vwj#AllOccurrences(string, pattern)
    " (reminder: count() doesn't handle regexes, so can't use it here,
    " and in general the absence of a string function that would return
    " a list with indexes for all the matches of a pattern is appalling)
    let string = a:string
    let occurrences = []
    let previous = 0
    while v:true
        let match = matchstrpos(string, a:pattern)
        if match[0] == ''
            break
        endif
        call add(occurrences, match[1] + 1 + previous)
        let string = slice(string, match[2])
        let previous += match[2]
    endwhile
    return occurrences
endfunction

" Main
function! vwj#InitLabels()
    " get the screen lines
    let lines = range(line('w0'), line('w$'))
    " populate the dict of lines and columns containing the pattern, {'line1': [col1, col2, ...], ...}
    let lines_columns = map(copy(lines), '{str2nr(v:val): vwj#AllOccurrences(getline(1 + v:val - 1), g:vwj_pattern)}')->reduce({acc, val -> extend(acc, val)})
    " count the total number of the pattern occurrences
    let lines_columns_total = values(lines_columns)->flatten()->len()
    " select either the necessary amount of labels to generate, or just limit the amount of the pattern occurrences (if there can't be generated enough labels)
    let lines_columns_total = min([len(g:vwj_labels) * len(g:vwj_labels), lines_columns_total])
    " generate the necessary amount of labels
    let labels = map(range(0, lines_columns_total - 1), {key -> g:vwj_labels[key/len(g:vwj_labels)] . g:vwj_labels[key%len(g:vwj_labels)]})

    " init the dict to store all the generated text properties
    let b:props = {}
    " init the dict to store all the generated popups (not that it is needed, though)
    let b:labels = {}
    " init the text properties identifier
    let prop_id = 0

    " for every line from the sorted array of lines containing the pattern occurrences
    for line in keys(lines_columns)->sort('N')
        " skip fodled lines
        if foldclosed(line) != -1
            continue
        endif
        " and for every pattern occurrences from the current line
        for i in range(0, len(lines_columns[line]) - 1)
            " generate empty text properties at the pattern occurrences
            let b:props[labels[prop_id]] = prop_add(line, lines_columns[line][i], #{type: 'vwj_empty', id: prop_id})
            " then attach popups to those epty text properties
            let b:labels[labels[prop_id]] = popup_create(labels[prop_id], #{line: -1, textprop: 'vwj_empty', textpropid: prop_id, highlight: g:vwj_colors})
            " and then increment the text properties identifier
            let prop_id += 1
        endfor
    endfor
endfunction

function! vwj#GotoLabels()
    " get the user input
    echo "Jump to label? "
    let lab1 = getchar()->nr2char()
    if "qwertyuiopasdfghjklzxcvbnm" =~ lab1
        let lab2 = getchar()->nr2char()
        if "qwertyuiopasdfghjklzxcvbnm" =~ lab2
            let lab = toupper(lab1 . lab2)
        else
            let lab = ''
        endif
    else
        let lab = ''
    endif

    " if a label was indeed provided
    if lab != ''
        try
            " extract its position from the dict of the generated text properties
            let p = prop_find({'lnum': 1, 'id': b:props[lab]})
            " clear all the text properties
            call prop_clear(1, line('$'))
            " jump to the obtained position
            call setpos('.', [0, p.lnum, p.col])
        catch /E716/
        endtry
    " otherwise
    else
        " just clear all the text properties
        call prop_clear(1, line('$'))
    endif
    call popup_clear()
endfunction

function! vwj#VWJ()
    call vwj#InitLabels()
    redraw!
    call vwj#GotoLabels()
    redraw!
endfunction
