" Init
if exists('g:vwj_loaded')
    finish
endif
let g:vwj_loaded = 1

if !exists("g:vwj_empty")
    call prop_type_add('vwj_empty', {})
    let g:vwj_empty = 1
endif
if !exists("g:vwj_pattern")
    let g:vwj_pattern = '\<.\{-}\>'
endif
if !exists("g:vwj_labels")
    let g:vwj_labels = ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "A", "S", "D", "F", "G", "H", "J", "K", "L", "Z", "X", "C", "V", "B", "N", "M"]
endif
if !exists("g:vwj_colors")
    let g:vwj_colors='Pmenu'
endif

" Command
command! VWJ call vwj#VWJ()

" Mapping
nnoremap <Plug>(vwj) :call vwj#VWJ()<CR>
